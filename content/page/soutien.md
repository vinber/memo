## Soutien

Le texte qui suit a été nourri de la contribution d'une trentaine de personnes œuvrant aujourd'hui en se fondant sur les principes communs de la libre circulation de l'information, de gouvernance ouverte et de modèles sociaux durables.

Plusieurs organisations ont traduit leur soutien par la publication du mémorandum et/ou des enseignements et mesures sur leur site.

Liste des organisations :
- [ADOA](http://adoa.solutions/)
- [ADULLACT](https://adullact.org/)
- [Ça reste ouvert](https://caresteouvert.fr)
- [Covid-initiatives.org](https://covid-initiatives.org/)
- [Framasoft](https://framasoft.org/fr/)
- [HackYourResearch](https://hackyourphd.org/)
- [Inno³](https://inno3.fr/)
- [La Fabrique des Mobilités](http://lafabriquedesmobilites.fr/)
- [La MYNE](https://www.lamyne.org/)
- LibreAccès
- [NAOS](http://naos-cluster.com/)
- [OpenDataFrance](http://www.opendatafrance.net/)
- [Wikimedia France](https://www.wikimedia.fr/)

Pour contacter les initiatives participantes : covid19-open-memo@framalistes.org
