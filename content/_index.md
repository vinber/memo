En cette période de crise et de confinement, de nombreuses initiatives portées par les mouvements du libre, de l’*open (source/science/data/hardware)* et des communs ont été (re)découvertes pour répondre aux besoins numériques repensés au regard des enjeux sociaux actuels. Complémentaires ou alternatifs aux dispositifs publics, ces courants communautaires de solidarité témoignent ainsi de leur pertinence à répondre à des besoins d’organisation rapide de manière pragmatique.

Par ce mémorandum commun, nous souhaitons :

- rappeler en quoi **ces initiatives sont particulièrement nécessaires dans la situation actuelle**, mais aussi pourquoi **elles impliquent une réflexion sur nos organisations sociales** qu'il s'agit de construire avec différentes parties prenantes (acteurs proches, grand public, acteurs du numérique et gouvernement/politique) ;

- participer à la **construction d'une vision commune sur la place du libre, de l'open et des communs** en ce temps de crise. **Rappeler nos valeurs et points communs sera utile pour donner du sens aux actions collectives futures**, face aux utilisateurs et utilisatrices de ces solutions et aux décideurs / politiques qui en cette période revoient nécessairement leurs idées préconçues.


Par « nous », nous entendons les communautés œuvrant aujourd'hui en se fondant sur les principes communs de la libre circulation de l'information, de gouvernance ouverte et de modèles sociaux durables.

Écrit durant la crise, le mémorandum rappelle l’importance et la place des initiatives ouvertes, participatives et collaboratives pour notre société. Il permettra de se souvenir (**6 enseignements sont soulignés**) mais aussi d’inspirer nos politiques publiques à venir. Devant l’urgence, il se double de **7 mesures immédiatement envisageables**.

## Organisation du projet

Sur ce site vous trouverez :
  - le [mémorandum](./page/memo)
  - les [impulsions](./page/impulsions) soit 6 enseignements et 7 mesures politiques immédiatement planifiables
  - les [soutiens](./page/soutien) soit les organisations qui ont publié sur leur site le mémorandum
  - une page [ressources](./page/ressources) référençant un certain nombre d'initiatives de veille consacrées à ce sujet.

## Participation

Si vous souhaitez contribuer, soumettre des modifications au mémorandum, soutenir l'intiative. Vous pouvez directement le faire via une demande de merge (*merge request*) sur le [répertoire correspondant](https://framagit.org/covid19-open/memo) ou en contactant directement les soutiens à ce projet (covid19-open-memo@framalistes.org).


## Mention légale du site  

Le contenu du site est mis à disposition selon les termes de la [Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).
Le code source de la page est disponible sur [framagit](https://framagit.org/covid19-open/memo).
